﻿Imports System
Imports System.Collections.Generic
Imports System.Linq

Namespace AdventOfCode2021
    Public Class Day10
        Inherits DayBase

        Public Overrides ReadOnly Property DayNumber As Integer = 10

        Dim nav As Navigation

        Public Overrides Function PartOne() As Long
            Prepare()

            Dim score As Integer = 0
            For Each line As Chunk In nav.lines
                score += DirectCast(line.FindFirstCorruptBracket(), Integer)
            Next

            Return score
        End Function

        Public Overrides Function PartTwo() As Long
            Prepare()

            Dim scores As New List(Of ULong)
            For Each line As Chunk In nav.lines
                If line.FindFirstCorruptBracket() > 0 Then Continue For

                scores.Add(line.Autocomplete() / 5) ' Off-by-one error on the *5-ing, so just fix it here.
            Next

            Dim middle As Long
            If scores.Count > 1 Then
                scores.Sort()
                Debug(scores.Count & " Scores found: " & String.Join(",", scores))
                middle = scores(scores.Count / 2) ' int(5/2) = 2, which is the third item in a zero-indexed list.
            Else
                middle = scores(0)
            End If

            Return middle
        End Function

        Private Sub Prepare()
            nav = New Navigation
            Dim thisChunk As Chunk
            For Each line As String In GetInput()
                thisChunk = New Chunk(Nothing) 'i.e. is root.
                thisChunk.SetListener(Me)
                nav.lines.Add(thisChunk)
                Debug(line)

                For i As Integer = 0 To line.Length - 1
                    Select Case line.Substring(i, 1)
                        Case "("
                            thisChunk = thisChunk.AddChild()
                            thisChunk.type = Chunk.eType.Round
                        Case "["
                            thisChunk = thisChunk.AddChild()
                            thisChunk.type = Chunk.eType.Square
                        Case "{"
                            thisChunk = thisChunk.AddChild()
                            thisChunk.type = Chunk.eType.Curly
                        Case "<"
                            thisChunk = thisChunk.AddChild()
                            thisChunk.type = Chunk.eType.Angle

                        Case ")"
                            If thisChunk.type = Chunk.eType.Round Then
                                thisChunk.status = Chunk.eStatus.Valid
                            Else
                                Debug("Found ), expecting " & Chunk.Type_ToString(thisChunk.type).Substring(1) & " at char " & i)
                                thisChunk.status = Chunk.eStatus.Corrupt
                                thisChunk.type = Chunk.eType.Round
                                Exit For
                            End If
                            thisChunk = thisChunk.GetParent
                        Case "]"
                            If thisChunk.type = Chunk.eType.Square Then
                                thisChunk.status = Chunk.eStatus.Valid
                            Else
                                Debug("Found ], expecting " & Chunk.Type_ToString(thisChunk.type).Substring(1) & " at char " & i)
                                thisChunk.status = Chunk.eStatus.Corrupt
                                thisChunk.type = Chunk.eType.Square
                                Exit For
                            End If
                            thisChunk = thisChunk.GetParent
                        Case "}"
                            If thisChunk.type = Chunk.eType.Curly Then
                                thisChunk.status = Chunk.eStatus.Valid
                            Else
                                Debug("Found }, expecting " & Chunk.Type_ToString(thisChunk.type).Substring(1) & " at char " & i)
                                thisChunk.status = Chunk.eStatus.Corrupt
                                thisChunk.type = Chunk.eType.Curly
                                Exit For
                            End If
                            thisChunk = thisChunk.GetParent
                        Case ">"
                            If thisChunk.type = Chunk.eType.Angle Then
                                thisChunk.status = Chunk.eStatus.Valid
                            Else
                                Debug("Found >, expecting " & Chunk.Type_ToString(thisChunk.type).Substring(1) & " at char " & i)
                                thisChunk.status = Chunk.eStatus.Corrupt
                                thisChunk.type = Chunk.eType.Angle
                                Exit For
                            End If
                            thisChunk = thisChunk.GetParent
                    End Select
                Next
            Next
        End Sub


        Public Class Navigation
            Public lines As List(Of Chunk)

            Public Sub New()
                lines = New List(Of Chunk)
            End Sub

        End Class

        Public Class Chunk
            Private parent As Chunk
            Private listener As Day10
            Public chunks As List(Of Chunk)
            Public type As eType
            Public status As eStatus

            Public Sub New(parent As Chunk)
                Me.parent = parent
                Me.chunks = New List(Of Chunk)
                Me.type = eType.None
                Me.status = eStatus.Incomplete
                If parent IsNot Nothing Then Me.SetListener(parent.listener)
            End Sub
            Public Sub SetListener(listener As Day10)
                Me.listener = listener
            End Sub

            Public Function GetParent() As Chunk
                If parent Is Nothing Then Return Me
                Return parent
            End Function

            Public Function AddChild() As Chunk
                Dim child As New Chunk(Me)
                chunks.Add(child)
                Return child
            End Function


            Public Function FindFirstCorruptBracket() As Chunk.eType
                Dim corrupt As eType
                For Each child As Chunk In chunks
                    corrupt = child.FindFirstCorruptBracket
                    If corrupt <> eType.None Then
                        Return corrupt
                    End If
                Next

                If Me.status = eStatus.Corrupt Then
                    listener.Debug("Found illegal char: " & Chunk.Type_ToString(Me.type).Substring(1))
                    Return Me.type
                End If

                Return eType.None
            End Function

            Public Function Autocomplete() As ULong
                Dim score As ULong = 0

                If chunks.Count > 0 Then
                    If chunks.Last.status = eStatus.Incomplete Then
                        score = chunks.Last.Autocomplete()
                        listener.Debug("Previous score = " & score) ' & " * 5 = " & score * 5)
                    End If
                End If


                If Me.status = eStatus.Incomplete Then
                    listener.Debug("Score = " & score & " * 5 = " & score * 5)
                    score *= 5

                    Select Case Me.type
                        Case eType.Round
                            listener.Debug("Autocomplete with ) => " & score & " + 1 = " & (score + 1)) '& " * 5 = " & ((score + 1) * 5))
                            score += 1
                        Case eType.Square
                            listener.Debug("Autocomplete with ] => " & score & " + 2 = " & (score + 2)) ' & " * 5 = " & ((score + 2) * 5))
                            score += 2
                        Case eType.Curly
                            listener.Debug("Autocomplete with } => " & score & " + 3 = " & (score + 3)) ' & " * 5 = " & ((score + 3) * 5))
                            score += 3
                        Case eType.Angle
                            listener.Debug("Autocomplete with > => " & score & " + 4 = " & (score + 4)) ' & " * 5 = " & ((score + 4) * 5))
                            score += 4
                    End Select
                End If

                Return score
            End Function


            Public Enum eType
                None = 0 '      unknown
                Round = 3 '     ( )
                Square = 57 '   [ ]
                Curly = 1197 '  { }
                Angle = 25137 ' < >
            End Enum

            Public Enum eStatus
                Incomplete = 0 'by default: is incomplete until closed.
                Valid
                Corrupt
            End Enum

            Public Shared Function Type_ToString(type As eType) As String
                Select Case type
                    Case eType.Round
                        Return "()"
                    Case eType.Square
                        Return "[]"
                    Case eType.Curly
                        Return "{}"
                    Case eType.Angle
                        Return "<>"
                End Select
                Return "??"
            End Function

        End Class



    End Class
End Namespace
