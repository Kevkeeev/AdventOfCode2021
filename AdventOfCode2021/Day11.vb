﻿Imports System
Imports System.Collections.Generic

Namespace AdventOfCode2021
    Public Class Day11
        Inherits DayBase

        Public Overrides ReadOnly Property DayNumber As Integer = 11

        Public swarm As OctopusSwarm

        Public Overrides Function PartOne() As Long
            Prepare()

            Dim flashes As Long = 0
            For i As Integer = 1 To 100
                Dim increase As Long = swarm.Step()

                If cDebug Then
                    Debug("After step " & i & ": " & increase & " flashes.")
                    For j As Integer = 0 To Me.swarm.octopus.Count - 1
                        Debug("" & String.Join("", swarm.octopus(j)))
                    Next
                    Debug("")
                End If

                flashes += increase
            Next

            Return flashes
        End Function

        Public Overrides Function PartTwo() As Long
            Prepare()

            Dim swarmSize As Long = swarm.octopus.Count * swarm.octopus(0).Count
            Debug("Swarm of size " & swarm.octopus.Count & " * " & swarm.octopus(0).Count & " = " & swarmSize)

            Dim flashes As Integer
            Dim answer As Long = 1
            While True
                flashes = swarm.Step()
                If flashes = swarmSize Then Exit While
                answer += 1
            End While

            Return answer
        End Function

        Private Sub Prepare()
            swarm = New OctopusSwarm(Me)

            For Each line As String In GetInput()
                swarm.octopus.Add(New List(Of Octopus))
                For i As Integer = 0 To line.Length - 1
                    swarm.octopus(swarm.octopus.Count - 1).Add(New Octopus(Integer.Parse(line.Substring(i, 1))))
                Next
            Next
        End Sub

        Public Class Octopus
            Public flashed As Boolean
            Public energy As Integer

            Public Sub New(energy As Integer)
                Me.flashed = False
                Me.energy = energy
            End Sub

            Public Sub Flash()
                Me.flashed = True
                Me.energy = 0
            End Sub

            Public Sub Reset()
                Me.flashed = False
                If Me.energy > 9 Then Me.energy = 0
            End Sub

            Public Overrides Function ToString() As String
                Return energy.ToString
            End Function

        End Class

        Public Class OctopusSwarm
            Public octopus As List(Of List(Of Octopus))
            Private listener As Day11

            Public Sub New(listener As Day11)
                Me.listener = listener
                octopus = New List(Of List(Of Octopus))
            End Sub

            Public Function [Step]() As Integer
                Dim flashes As Integer = 0

                For x As Integer = 0 To octopus.Count - 1
                    For y As Integer = 0 To octopus(x).Count - 1
                        octopus(x)(y).energy += 1
                    Next
                Next

                For x As Integer = 0 To octopus.Count - 1
                    For y As Integer = 0 To octopus(x).Count - 1
                        flashes += CheckFlash(x, y)
                    Next
                Next

                For x As Integer = 0 To octopus.Count - 1
                    For y As Integer = 0 To octopus(x).Count - 1
                        octopus(x)(y).reset()
                    Next
                Next


                Return flashes
            End Function

            Public Function CheckFlash(x As Integer, y As Integer) As Integer
                'listener.Debug("Octopus: x=" & x & ", y=" & y & ", e=" & octopus(x)(y).energy & ", f=" & octopus(x)(y).flashed)

                Dim flashes As Integer = 0
                If octopus(x)(y).energy > 9 AndAlso Not octopus(x)(y).flashed Then
                    octopus(x)(y).Flash()
                    flashes += 1

                    If y > 0 Then
                        'check top-left
                        If x > 0 Then
                            If Not octopus(x - 1)(y - 1).flashed Then
                                octopus(x - 1)(y - 1).energy += 1
                                flashes += CheckFlash(x - 1, y - 1)
                            End If
                        End If

                        'check top
                        If Not octopus(x)(y - 1).flashed Then
                            octopus(x)(y - 1).energy += 1
                            flashes += CheckFlash(x, y - 1)
                        End If

                        'check top-right
                        If x < octopus.Count - 1 Then
                            If Not octopus(x + 1)(y - 1).flashed Then
                                octopus(x + 1)(y - 1).energy += 1
                                flashes += CheckFlash(x + 1, y - 1)
                            End If
                        End If
                    End If 'Top row

                    'check left
                    If x > 0 Then
                        If Not octopus(x - 1)(y).flashed Then
                            octopus(x - 1)(y).energy += 1
                            flashes += CheckFlash(x - 1, y)
                        End If
                    End If

                    'check right
                    If x < octopus.Count - 1 Then
                        If Not octopus(x + 1)(y).flashed Then
                            octopus(x + 1)(y).energy += 1
                            flashes += CheckFlash(x + 1, y)
                        End If
                    End If

                    If y < octopus(x).Count - 1 Then
                        'check bottom-left
                        If x > 0 Then
                            If Not octopus(x - 1)(y + 1).flashed Then
                                octopus(x - 1)(y + 1).energy += 1
                                flashes += CheckFlash(x - 1, y + 1)
                            End If
                        End If

                        'check bottom
                        If Not octopus(x)(y + 1).flashed Then
                            octopus(x)(y + 1).energy += 1
                            flashes += CheckFlash(x, y + 1)
                        End If

                        'chech bottom-right
                        If x < octopus.Count - 1 Then
                            If Not octopus(x + 1)(y + 1).flashed Then
                                octopus(x + 1)(y + 1).energy += 1
                                flashes += CheckFlash(x + 1, y + 1)
                            End If
                        End If
                    End If 'bottom row
                End If 'Flash

                Return flashes
            End Function

        End Class


    End Class
End Namespace
