﻿Imports System
Imports System.Collections.Generic
Imports System.Linq

Namespace AdventOfCode2021
    Public Class Day12
        Inherits DayBase

        Public Overrides ReadOnly Property DayNumber As Integer = 12

        Public map As CaveMap

        Public Overrides Function PartOne() As Long
            Prepare()

            If cDebug Then
                Debug("Caves: ")
                For Each cave As Cave In map.caves
                    Debug("Cave: " & cave.name & ", connected to: " & String.Join(",", cave.connectedCaves))
                Next
            End If

            Dim routes As List(Of CaveRoute) = map.GetCave("start").Traverse(New CaveRoute, Nothing)

            If cDebug Then
                For Each route As CaveRoute In routes
                    Debug(String.Join(",", route.connections))
                Next
            End If

            Return routes.Count
        End Function

        Public Overrides Function PartTwo() As Long
            Prepare()

            Dim routes As List(Of CaveRoute) = map.GetCave("start").Traverse(New CaveRoute, Nothing)

            If cDebug Then
                For Each route As CaveRoute In routes
                    Debug(String.Join(",", route.connections))
                Next
            End If

            Return routes.Count
        End Function

        Private Sub Prepare()
            map = New CaveMap

            Dim conn As Connection
            For Each line As String In GetInput
                conn = New Connection()
                For Each name As String In line.Split("-")
                    If Char.IsUpper(name, 0) Then 'assuming every letter in the name is the same case, checking the first is good enough
                        conn.Add(map.AddCave(New LargeCave(name, Me)))
                    Else
                        conn.Add(map.AddCave(New SmallCave(name, Me)))
                    End If
                Next

                conn.from.AddConnection(conn.to)
                conn.to.AddConnection(conn.from)
            Next
        End Sub

        Public Class CaveMap
            Public caves As List(Of Cave)

            Public Sub New()
                caves = New List(Of Cave)
            End Sub

            Public Function GetCave(name As String) As Cave
                For Each cave As Cave In caves
                    If cave.name = name Then Return cave
                Next
                Return Nothing
            End Function

            Public Function AddCave(cave As Cave) As Cave
                If Not caves.Contains(cave) Then caves.Add(cave)
                Return GetCave(cave.name) 'if we already added a cave with the same name, return the original one - not this copy.
            End Function
        End Class

        Public Class CaveRoute
            Public connections As List(Of Connection)
            Public smallCave As Cave 'to visit twice

            Public Sub New()
                Me.connections = New List(Of Connection)
                Me.smallCave = Nothing
            End Sub

            Public Sub New(previousRoute As CaveRoute)
                Me.New()
                For Each connection As Connection In previousRoute.connections
                    Me.connections.Add(connection)
                Next
                Me.smallCave = previousRoute.smallCave
            End Sub

            Public Function Contains(from As Cave, [to] As Cave) As Boolean
                Return Me.Contains(New Connection(from, [to]))
            End Function
            Public Function Contains(conn As Connection)
                Return Me.connections.Contains(conn)
            End Function

            Public Function HasVisitedSmall(cave As Cave) As Boolean
                If cave.isLarge Then Return False

                For Each conn As Connection In connections
                    If conn.to.name = cave.name Then
                        If {"start", "end"}.Contains(cave.name) Then Return True 'can NEVER visit either of these twice.

                        If Me.smallCave Is Nothing Then 'else: check if we may visit this cave twice.
                            'this is the first time we visit a small cave for the second time. We'll allow it this time.
                            smallCave = cave
                            Return False
                        Else
                            Return True
                        End If

                    End If
                Next
                Return False
            End Function

            Public Sub Add(from As Cave, [to] As Cave)
                Dim conn As New Connection(from, [to])
                If Not connections.Contains(conn) Then connections.Add(conn)
            End Sub

            Public Overrides Function ToString() As String
                Return String.Join(",", connections)
            End Function
        End Class

        Public Class Connection
            Public from As Cave
            Public [to] As Cave

            Public Sub Add(cave As Cave)
                If from Is Nothing Then
                    from = cave
                Else
                    [to] = cave
                End If
            End Sub

            Public Overrides Function ToString() As String
                Return [to].name 'because FROM will be in the previous TO.
            End Function

            Public Sub New()
            End Sub

            Public Sub New(from As Cave, [to] As Cave)
                Me.New()
                Me.from = from
                Me.to = [to]
            End Sub

            Public Function Contains(cave As Cave) As Boolean
                Return {from.name, [to].name}.Contains(cave.name)
            End Function
        End Class

        Public MustInherit Class Cave
            Implements IEquatable(Of Cave)

            Public Overridable ReadOnly Property isLarge As Boolean
            Public name As String
            Public connectedCaves As List(Of Cave)
            Private listener As Day12

            Public Sub New(name As String, listener As Day12)
                connectedCaves = New List(Of Cave)
                Me.name = name
                Me.listener = listener
            End Sub

            Public Overrides Function ToString() As String
                Return Me.name
            End Function

            Public Sub AddConnection([to] As Cave)
                If Not connectedCaves.Contains([to]) Then connectedCaves.Add([to])
            End Sub

            Public Function Traverse(route As CaveRoute, previous As Cave) As List(Of CaveRoute)
                Dim routes As New List(Of CaveRoute)

                'listener.Debug("Traversing " & Me.name & " (from: " & If(previous Is Nothing, "", previous.name) & "), connections: " & String.Join(",", connectedCaves))

                route.Add(previous, Me)

                If Me.name <> "end" Then 'we're not going back from the exit...
                    For Each cave As Cave In Me.connectedCaves
                        If Not route.HasVisitedSmall(cave) Then
                            routes.AddRange(cave.Traverse(New CaveRoute(route), Me))
                        End If 'no need to go from A-B to B-A to A-B again. That's just going back and forth.
                    Next
                Else
                    routes.Add(route)
                End If

                Return routes
            End Function

            Public Overloads Function Equals(other As Cave) As Boolean Implements IEquatable(Of Cave).Equals
                Return Me.name.Equals(other.name)
            End Function
        End Class

        Public Class SmallCave
            Inherits Cave
            Public Overrides ReadOnly Property isLarge As Boolean = False

            Public Sub New(name As String, listener As Day12)
                MyBase.New(name, listener)
            End Sub
        End Class

        Public Class LargeCave
            Inherits Cave
            Public Overrides ReadOnly Property isLarge As Boolean = True

            Public Sub New(name As String, listener As Day12)
                MyBase.New(name, listener)
            End Sub
        End Class
    End Class

End Namespace
