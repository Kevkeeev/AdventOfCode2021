﻿Imports System
Imports System.Collections.Generic
Imports System.Linq

Namespace AdventOfCode2021
    Public Class Day13
        Inherits DayBase

        Public Overrides ReadOnly Property DayNumber As Integer = 13

        Public paper As TransparentPaper
        Public folds As List(Of FoldInstruction)

        Public Overrides Function PartOne() As Long
            Prepare()

            paper.Print()

            For Each fold As FoldInstruction In folds
                paper.Fold(fold)
                Debug(paper.CountDots)
                Exit For
            Next

            Return paper.CountDots
        End Function

        Public Overrides Function PartTwo() As Long
            Prepare()

            For Each fold As FoldInstruction In folds
                paper.Fold(fold)
            Next

            cDebug = True 'force printing.
            paper.Print()
            Return 0
        End Function

        Private Sub Prepare()
            paper = New TransparentPaper(Me)
            folds = New List(Of FoldInstruction)

            Dim x, y, maxX As Integer
            For Each line As String In GetInput
                If line.Contains(",") Then
                    With line.Split(",")
                        x = Integer.Parse(.GetValue(0))
                        y = Integer.Parse(.GetValue(1))
                    End With

                    For i As Integer = 0 To y 'zero-indexed coordinates. x=0 means count=1
                        If paper.dots.Count <= i Then paper.dots.Add(New List(Of Boolean))
                        For j As Integer = 0 To Math.Max(x, maxX)
                            If paper.dots(i).Count <= j Then paper.dots(i).Add(False)
                        Next
                    Next

                    paper.dots(y)(x) = True
                    If maxX < x Then maxX = x
                ElseIf line.Contains("fold") Then
                    With line.Substring("fold along ".Length).Split("=")
                        folds.Add(New FoldInstruction(.GetValue(0), Integer.Parse(.GetValue(1))))
                    End With
                End If
            Next
        End Sub


        Public Class TransparentPaper
            Public dots As List(Of List(Of Boolean))
            Private listener As Day13

            Public Sub New(listener As Day13)
                dots = New List(Of List(Of Boolean))
                Me.listener = listener
            End Sub

            Public Function CountDots()
                Dim num As Integer = 0
                For Each line As List(Of Boolean) In dots
                    For i As Integer = 0 To line.Count - 1
                        If line(i) Then num += 1
                    Next
                Next
                Return num
            End Function

            Public Sub Print()
                Dim lines As New List(Of String)

                For i As Integer = 0 To dots.Count - 1
                    If lines.Count <= i Then lines.Add("")
                    For j As Integer = 0 To dots(i).Count - 1
                        lines(i) &= If(dots(i)(j), "#", ".")
                    Next
                Next

                For Each line As String In lines
                    listener.Debug(line)
                Next
            End Sub

            Public Sub Fold(fold As FoldInstruction)
                listener.Debug("Folding: along " & fold.axis & " = " & fold.line)

                If fold.axis = "x" Then
                    Dim rightHalf As New List(Of List(Of Boolean))

                    For i As Integer = 0 To dots.Count - 1
                        rightHalf.Add(New List(Of Boolean))
                        For j As Integer = dots(i).Count - 1 To fold.line + 1 Step -1
                            rightHalf(i).Add(dots(i)(j))
                            dots(i).RemoveAt(j)
                        Next
                        dots(i).RemoveAt(fold.line)
                    Next

                    If cDebug Then
                        For i As Integer = 0 To rightHalf.Count - 1
                            For j As Integer = 0 To rightHalf(i).Count - 1
                                listener.Debug(If(rightHalf(i)(j), "X", "_"), "")
                            Next
                            listener.Debug("")
                        Next
                    End If

                    Dim offset As Integer = dots(0).Count - rightHalf(0).Count
                    For i As Integer = 0 To rightHalf.Count - 1
                        For j As Integer = 0 To rightHalf(i).Count - 1
                            dots(i)(j + offset) = dots(i)(j + offset) Or rightHalf(i)(j)
                        Next
                    Next

                ElseIf fold.axis = "y" Then
                    Dim bottomHalf As New List(Of List(Of Boolean))

                    For i As Integer = dots.Count - 1 To fold.line + 1 Step -1
                        bottomHalf.Add(dots(i))
                        dots.RemoveAt(i)
                    Next
                    dots.RemoveAt(fold.line) 'also remove the folding line

                    If cDebug Then
                        For i As Integer = 0 To bottomHalf.Count - 1
                            For j As Integer = 0 To bottomHalf(i).Count - 1
                                listener.Debug(If(bottomHalf(i)(j), "X", "_"), "")
                            Next
                            listener.Debug("")
                        Next
                    End If

                    Dim offset As Integer = dots.Count - bottomHalf.Count
                    'The FIRST line from bottomhalf (flipped up already) goes to the FIRST appropriate line on dots.
                    For i As Integer = 0 To bottomHalf.Count - 1
                        For j As Integer = 0 To bottomHalf(i).Count - 1
                            dots(i + offset)(j) = dots(i + offset)(j) Or bottomHalf(i)(j)
                        Next
                    Next
                End If

                Print()
            End Sub

        End Class

        Public Class FoldInstruction
            Public axis As String
            Public line As Integer

            Public Sub New(axis As String, line As Integer)
                Me.axis = axis
                Me.line = line
            End Sub
        End Class
    End Class
End Namespace
