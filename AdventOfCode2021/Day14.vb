﻿Imports System
Imports System.Collections.Generic
Imports System.Linq

Namespace AdventOfCode2021
    Public Class Day14
        Inherits DayBase

        Public Overrides ReadOnly Property DayNumber As Integer = 14

        Public polymer As String
        Public inserts As List(Of Insertion)

        Public Overrides Function PartOne() As Long
            Return PerformSteps(10)
        End Function

        Public Overrides Function PartTwo() As Long
            Return PerformSteps(40)
        End Function

        Public Function PerformSteps(num As Integer, Optional forceStatus As Boolean = True) As Long
            Prepare()

            For i As Integer = 1 To num
                Dim actions As New ActionList(Me)

                For Each insert In inserts
                    For Each pair As KeyValuePair(Of Integer, String) In FindInserts(polymer, insert)
                        actions.AddInsert(pair.Key, pair.Value)
                        actions.Sort()
                        Debug("Actions = " & String.Join(",", actions.list))
                    Next
                Next

                actions.Sort()
                For Each pair As KeyValuePair(Of Integer, String) In actions.list
                    polymer = polymer.Insert(pair.Key, pair.Value)
                Next
                Debug("After step " & i & ": " & polymer)
                Debug("")

                If forceStatus Then Console.WriteLine(Date.Now.ToString("yyyy-MM-dd HH:mm:ss") & " Step " & i & " finsihed.")
            Next

            Dim dict As New List(Of KeyValuePair(Of Char, Integer))
            Dim min As Integer = Integer.MaxValue
            Dim max As Integer = Integer.MinValue

            For j As Integer = 1 To 26 ' loop over all letters of the alphabet, and convert them to ascii
                Dim letter As Char = Char.ConvertFromUtf32(j + 64) ' A = 65.
                Dim count As Integer = polymer.Count(Function(c) c = letter)
                dict.Add(New KeyValuePair(Of Char, Integer)(letter, count))
                If count > 0 Then
                    Debug(letter & " = " & count)
                    If count < min Then min = count
                    If count > max Then max = count
                End If
            Next

            Debug("max = " & max & ", min = " & min)
            Return max - min
        End Function


        Private Sub Prepare()
            inserts = New List(Of Insertion)
            For Each line As String In GetInput
                If polymer = "" Then
                    polymer = line
                ElseIf line <> "" Then
                    With line.Split(" -> ")
                        inserts.Add(New Insertion(.GetValue(0), .GetValue(1)))
                    End With
                End If
            Next
        End Sub

        Public Function FindInserts(polymer As String, insert As Insertion) As Dictionary(Of Integer, String)
            Dim found As New Dictionary(Of Integer, String)

            For i As Integer = 0 To polymer.Length - 2 'we won't check the last element AS a first element.
                If polymer.Substring(i, 2) = insert.pair Then
                    found.Add(i + 1, insert.insert) ' calculate the NEW index we insert the new element.
                End If
            Next

            If found.Count > 0 Then Debug("Looking for pair " & insert.pair & " to insert " & insert.insert & " in " & polymer & ", found: " & String.Join(",", found))

            Return found
        End Function

        Public Class Insertion
            Public pair As String
            Public insert As String

            Public Sub New(pair As String, insert As String)
                Me.pair = pair
                Me.insert = insert
            End Sub
        End Class

        Public Class ActionList
            Public list As List(Of KeyValuePair(Of Integer, String))
            Private listener As Day14

            Public Sub New(listener As Day14)
                list = New List(Of KeyValuePair(Of Integer, String))
                Me.listener = listener
            End Sub

            Public Sub Sort()
                list.Sort(Function(x, y) x.Key.CompareTo(y.Key))
            End Sub

            Public Sub AddInsert(index As Integer, element As String)
                Sort()

                For i As Integer = 0 To list.Count - 1
                    If list(i).Key <= index Then
                        'there's already an insert before (or at) THIS index.
                        index += 1
                    Else
                        'there's also an insert AFTER this index. increase THAT index too.
                        list(i) = New KeyValuePair(Of Integer, String)(list(i).Key + 1, list(i).Value)
                    End If
                Next

                list.Add(New KeyValuePair(Of Integer, String)(index, element))
            End Sub
        End Class

    End Class
End Namespace
