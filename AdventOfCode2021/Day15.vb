﻿Imports System
Imports System.Collections.Generic
Imports System.Linq

Namespace AdventOfCode2021
    Public Class Day15
        Inherits DayBase

        Public Overrides ReadOnly Property DayNumber As Integer = 15

        Public map As CavernMap

        Public Overrides Function PartOne() As Long
            Prepare()

            Dim [end] As Point = New Point(map.map(0).Count - 1, map.map.Count - 1)
            Dim routes As List(Of CavernRoute) = map.FindRoutes(New Point(0, 0), [end])

            routes.Sort(Function(x, y) x.GetScore.CompareTo(y.GetScore))

            Debug("Found " & routes.Count & " routes.")

            Return routes.First.GetScore
        End Function

        Public Overrides Function PartTwo() As Long
            For Each line As String In GetInput()
            Next

            Return -1
        End Function


        Private Sub Prepare()
            map = New CavernMap

            For Each line As String In GetInput
                map.map.Add(New List(Of Integer))
                For i As Integer = 0 To line.Length - 1
                    map.map(map.map.Count - 1).Add(Integer.Parse(line.Substring(i, 1)))
                Next
            Next
        End Sub

        Public Class CavernMap
            Public map As List(Of List(Of Integer))
            Private start, [end] As Point

            Public Sub New()
                map = New List(Of List(Of Integer))
            End Sub

            Public Function FindRoutes(start As Point, [end] As Point) As List(Of CavernRoute)
                Me.start = start
                Me.end = [end]

                Dim routes As New List(Of CavernRoute)

                'for each point, visit the points besides it.
                routes.AddRange(Traverse(start, New CavernRoute(Me)))

                Return routes
            End Function

            Public Function Traverse(p As Point, route As CavernRoute) As List(Of CavernRoute)
                Dim routes As New List(Of CavernRoute)

                route.Add(p)

                If p.Equals(Me.end) Then
                    routes.Add(route)
                Else
                    'check top
                    If p.y > 0 Then
                        If Not route.HasVisited(p.x, p.y - 1) Then
                            routes.AddRange(Traverse(New Point(p.x, p.y - 1), New CavernRoute(route)))
                        End If
                    End If

                    'check bottom
                    If p.y < map.Count - 1 Then
                        If Not route.HasVisited(p.x, p.y + 1) Then
                            routes.AddRange(Traverse(New Point(p.x, p.y + 1), New CavernRoute(route)))
                        End If
                    End If

                    'check left
                    If p.x > 0 Then
                        If Not route.HasVisited(p.x - 1, p.y) Then
                            routes.AddRange(Traverse(New Point(p.x - 1, p.y), New CavernRoute(route)))
                        End If
                    End If

                    'check right
                    If p.x < map(p.y).Count - 1 Then
                        If Not route.HasVisited(p.x + 1, p.y) Then
                            routes.AddRange(Traverse(New Point(p.x + 1, p.y), New CavernRoute(route)))
                        End If
                    End If
                End If

                Return routes
            End Function

        End Class

        Public Class CavernRoute
            Public points As List(Of Point)
            Private map As CavernMap
            Private score As Integer = -1

            Public Sub New(map As CavernMap)
                points = New List(Of Point)
                Me.map = map
            End Sub

            Public Sub New(previousRoute As CavernRoute)
                Me.New(previousRoute.map)
                For Each p As Point In previousRoute.points
                    Me.points.Add(p)
                Next
            End Sub

            Public ReadOnly Property GetScore As Integer
                Get
                    If Me.score < 0 Then ComputeScore()
                    Return Me.score
                End Get
            End Property

            Public Sub ComputeScore()
                Dim score As Integer = 0

                For Each p As Point In points
                    score += map.map(p.y)(p.x)
                Next

                Me.score = score
            End Sub

            Public Function HasVisited(x As Integer, y As Integer)
                Return Contains(New Point(x, y))
            End Function
            Public Function Contains(p As Point)
                Return points.Contains(p)
            End Function

            Public Sub Add(p As Point)
                points.Add(p)
            End Sub
        End Class

        Public Class Point
            Implements IEquatable(Of Point)

            Public x, y As Integer

            Public Sub New(x As Integer, y As Integer)
                Me.x = x
                Me.y = y
            End Sub

            Public Overloads Function Equals(other As Point) As Boolean Implements IEquatable(Of Point).Equals
                Return (Me.x = other.x AndAlso Me.y = other.y)
            End Function
        End Class

    End Class
End Namespace
