﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text.RegularExpressions

Namespace AdventOfCode2021
    Public Class Day16
        Inherits DayBase

        Public Overrides ReadOnly Property DayNumber As Integer = 16

        Public Overrides Function PartOne() As Long
            Dim packet As Packet = Prepare()

            Debug(packet.ToString)

            Return packet.GetSumOfVersions()
        End Function

        Public Overrides Function PartTwo() As Long
            Dim packet As Packet = Prepare()

            Debug(packet.ToString)

            Return packet.Calculate()
        End Function


        Private Function Prepare() As Packet
            Dim hex As String = GetInput.First 'is only one line.
            Dim binary As String = ""
            For i As Integer = 0 To hex.Length - 1
                binary &= HexToBinary(hex.Substring(i))
            Next

            Debug("Preparing: " & FormatBinary(binary))

            Dim version, type As Integer
            version = Convert.ToInt32(binary.Substring(0, 3), 2) ' first three bits are version
            type = Convert.ToInt32(binary.Substring(3, 3), 2) ' second set of three bits are type

            Debug("Parsed v:" & binary.Substring(0, 3) & "=" & version & " t:" & binary.Substring(3, 3) & "=" & type)

            If type = 4 Then
                Return New LiteralPacket(Me, version, type, binary.Substring(6))
            Else
                Return New OperatorPacket(Me, version, type, binary.Substring(6))
            End If

            Throw New Exception("Unknown type:" & type)
        End Function

        Public MustInherit Class Packet
            Public version As Integer
            Public type As Integer
            Protected listener As Day16

            Public leftoverBinary As String

            Public Sub New(listener As Day16, version As Integer, type As Integer, binary As String)
                Me.listener = listener
                Me.version = version
                Me.type = type

                listener.Debug("Consuming: " & FormatBinary(binary))
            End Sub

            Public MustOverride Sub Consume(binary As String)

            Public MustOverride Function GetSumOfVersions() As Long

            Public MustOverride Function Calculate() As Long

            Public Overrides Function ToString() As String
                Return "Packet: version:" & version & ", type:" & type
            End Function
        End Class

        Public Class LiteralPacket
            Inherits Packet

            Public value As Long

            Public Sub New(listener As Day16, version As Integer, type As Integer, binary As String)
                MyBase.New(listener, version, type, binary)

                Me.Consume(binary)

                listener.Debug("Parsed value=" & Me.value)
            End Sub

            Public Overrides Sub Consume(binary As String)
                Dim bit As String
                Dim rawValue As String = ""

                For i As Integer = 0 To Math.Floor(binary.Length / 5) - 1 '18/5 = 3.x > 0..2, rest is zeroes
                    bit = binary.Substring(i * 5, 5)
                    rawValue &= bit.Substring(1)

                    If bit.StartsWith("0") Then
                        leftoverBinary = binary.Substring((i + 1) * 5)
                        Exit For
                    End If
                Next

                Me.value = Convert.ToInt64(rawValue, 2)

                listener.Debug("Finished: leftover = " & FormatBinary(Me.leftoverBinary))
            End Sub

            Public Overrides Function Calculate() As Long
                Return Me.value
            End Function

            Public Overrides Function GetSumOfVersions() As Long
                Return Me.version
            End Function

            Public Overrides Function ToString() As String
                Return "{ Literal" & MyBase.ToString() & ", value:" & value & "}"
            End Function
        End Class

        Public Class OperatorPacket
            Inherits Packet

            Public operation As Integer
            Public packets As List(Of Packet)

            Public Sub New(listener As Day16, version As Integer, type As Integer, binary As String)
                MyBase.New(listener, version, type, binary)
                Me.packets = New List(Of Packet)

                Me.Consume(binary)
            End Sub

            Public Overrides Sub Consume(binary As String)
                Dim bits As String
                Dim lengthType As String = binary.Substring(0, 1)
                Dim version, type As Integer

                listener.Debug("Parsed i=" & lengthType)

                If lengthType = "0" Then
                    bits = binary.Substring(1, 15)
                    Dim length As Integer = Convert.ToInt32(bits, 2)

                    bits = binary.Substring(16, length)
                    Me.leftoverBinary = binary.Substring(16 + length)

                    While bits <> ""
                        version = Convert.ToInt32(bits.Substring(0, 3), 2) ' first three bits are version
                        type = Convert.ToInt32(bits.Substring(3, 3), 2) ' second set of three bits are type

                        listener.Debug("Parsed v:" & bits.Substring(0, 3) & "=" & version & " t:" & bits.Substring(3, 3) & "=" & type)

                        If type = 4 Then
                            Dim literal As New LiteralPacket(Me.listener, version, type, bits.Substring(6))
                            packets.Add(literal)
                            bits = literal.leftoverBinary
                        Else
                            Dim [operator] As New OperatorPacket(Me.listener, version, type, bits.Substring(6))
                            packets.Add([operator])
                            bits = [operator].leftoverBinary
                        End If
                    End While

                Else
                    bits = binary.Substring(1, 11)
                    Dim number As Integer = Convert.ToInt32(bits, 2)
                    bits = binary.Substring(12)

                    For i As Integer = 1 To number 'one-based
                        version = Convert.ToInt32(bits.Substring(0, 3), 2) ' first three bits are version
                        type = Convert.ToInt32(bits.Substring(3, 3), 2) ' second set of three bits are type

                        listener.Debug("Parsed v:" & bits.Substring(0, 3) & "=" & version & " t:" & bits.Substring(3, 3) & "=" & type)

                        If type = 4 Then
                            Dim literal As New LiteralPacket(Me.listener, version, type, bits.Substring(6))
                            packets.Add(literal)
                            bits = literal.leftoverBinary
                        Else
                            Dim [operator] As New OperatorPacket(Me.listener, version, type, bits.Substring(6))
                            packets.Add([operator])
                            bits = [operator].leftoverBinary
                        End If
                    Next

                    Me.leftoverBinary = bits
                End If

                listener.Debug("Finished: leftover = " & FormatBinary(Me.leftoverBinary))
            End Sub

            Public Overrides Function Calculate() As Long
                Dim value As Long = 0
                Select Case Me.type
                    Case 0 ' sum
                        listener.Debug("Calculating sum. ")
                        For Each packet As Packet In packets
                            value += packet.Calculate
                        Next
                        listener.Debug("Sum was " & value)

                    Case 1 ' product
                        value = 1 '0 * anything = 0...
                        listener.Debug("Calculating product. ")
                        For Each packet As Packet In packets
                            value *= packet.Calculate
                        Next
                        listener.Debug("Product was " & value)

                    Case 2 'min
                        value = Long.MaxValue
                        listener.Debug("Calculating min. ")
                        For Each packet As Packet In packets
                            Dim tmp As Long = packet.Calculate
                            If tmp < value Then value = tmp
                        Next
                        listener.Debug("Min was " & value)

                    Case 3 'max
                        value = Long.MinValue
                        listener.Debug("Calculating max. ")
                        For Each packet As Packet In packets
                            Dim tmp As Long = packet.Calculate
                            If tmp > value Then value = tmp
                        Next
                        listener.Debug("Max was " & value)

                        '4 = literal
                    Case 5 'greater than
                        listener.Debug("Calculating >. ")
                        Dim first, second As Long
                        first = packets.First.Calculate
                        second = packets.Last.Calculate
                        value = If(first > second, 1, 0)
                        listener.Debug("> was " & value)

                    Case 6 'less than
                        listener.Debug("Calculating <. ")
                        Dim first, second As Long
                        first = packets.First.Calculate
                        second = packets.Last.Calculate
                        value = If(first < second, 1, 0)
                        listener.Debug("< was " & value)

                    Case 7 'equal
                        listener.Debug("Calculating =. ")
                        Dim first, second As Long
                        first = packets.First.Calculate
                        second = packets.Last.Calculate
                        value = If(first = second, 1, 0)
                        listener.Debug("= was " & value)

                    Case Else
                        Throw New Exception("Type: " & type & " is not a supported operation!")
                End Select

                Return value
            End Function

            Public Overrides Function GetSumOfVersions() As Long
                Dim version As Long = Me.version
                For Each packet As Packet In Me.packets
                    version += packet.GetSumOfVersions
                Next
                Return version
            End Function

            Public Overrides Function ToString() As String
                Return "{ Operator" & MyBase.ToString() & ", operators: [ " & String.Join(", ", packets) & "] }"
            End Function
        End Class


        Public Shared Function FormatBinary(binary As String) As String
            Return "]" & Regex.Replace(New String(" ", 4 - binary.Length Mod 4) & binary, ".{4}", "$0 ").Trim & "[."
        End Function

        Public Function HexToBinary(hex As Char) As String
            Select Case hex
                Case "0"
                    Return "0000"
                Case "1"
                    Return "0001"
                Case "2"
                    Return "0010"
                Case "3"
                    Return "0011"
                Case "4"
                    Return "0100"
                Case "5"
                    Return "0101"
                Case "6"
                    Return "0110"
                Case "7"
                    Return "0111"
                Case "8"
                    Return "1000"
                Case "9"
                    Return "1001"
                Case "a", "A"
                    Return "1010"
                Case "b", "B"
                    Return "1011"
                Case "c", "C"
                    Return "1100"
                Case "d", "D"
                    Return "1101"
                Case "e", "E"
                    Return "1110"
                Case "f", "F"
                    Return "1111"
            End Select
            Throw New Exception("Unknown hex value: " & hex & "!")
        End Function

    End Class
End Namespace
