﻿Imports System
Imports System.Collections.Generic
Imports System.Linq

Namespace AdventOfCode2021
    Public Class Day18
        Inherits DayBase

        Public Overrides ReadOnly Property DayNumber As Integer = 18

        Public homework As List(Of SmallFishPair)

        Public Overrides Function PartOne() As Long
            Prepare()


            Return -1
        End Function

        Public Overrides Function PartTwo() As Long
            For Each line As String In GetInput()
            Next

            Return -1
        End Function

        Private Sub Prepare()
            Me.homework = New List(Of SmallFishPair)
            For Each line As String In GetInput
                homework.Add(New SmallFishPair(Me, line, Nothing))
                Debug("Read in: " & homework.Last.ToString)
            Next 'the outermost item is always a pair.
        End Sub


        Public MustInherit Class SmallFishObject
            Public leftoverInput As String
            Public listener As Day18

            Public Sub New(listener As Day18, input As String)
                Me.listener = listener
                listener.Debug("Parsing: '" & input & "'")
            End Sub

        End Class

        Public Class SmallFishValue
            Inherits SmallFishObject

            Public value As Long

            Public Sub New(listener As Day18, input As String)
                MyBase.New(listener, input)

                Dim iComma As Long = input.IndexOf(",")
                Dim iBracket As Long = input.IndexOf("]")
                Dim i As Long
                If iComma >= 0 AndAlso iBracket >= 0 Then 'both are found, grab the lowest
                    i = Math.Min(iComma, iBracket)
                Else 'at least one == -1, grab the highest
                    i = Math.Max(iComma, iBracket)
                End If
                listener.Debug("Found , at " & iComma & ", ] at " & iBracket & " in '" & input & "'. First = " & i)

                Dim num As String = If(i > 0, input.Substring(0, i), input)

                Try
                    Me.value = Long.Parse(num)
                Catch ex As Exception
                    listener.Debug("Could not parse value: '" & num & "' from input: '" & input & "'")
                    Throw 'rethrow to keep stacktrace intact.
                End Try

                Me.leftoverInput = input.Substring(i + 1) 'also remove comma or bracket that follows the value.
                listener.Debug("Finished. leftover = '" & Me.leftoverInput & "'")
            End Sub

            Public Overrides Function ToString() As String
                Return value
            End Function

        End Class

        Public Class SmallFishPair
            Inherits SmallFishObject

            Public parent As SmallFishPair
            Public left, right As SmallFishObject

            Public ReadOnly Property Level As Long
                Get
                    Return If(parent IsNot Nothing, parent.Level, -1) + 1 ' zero based >> 0 = root.
                End Get
            End Property

            Public Sub New(listener As Day18, input As String, parent As SmallFishPair)
                MyBase.New(listener, input)
                Me.parent = parent

                'Apparently, I'm a pair.
                If input.StartsWith("[") Then input = input.Substring(1) 'remove the first [

                If input.StartsWith("[") Then
                    Me.left = New SmallFishPair(listener, input, Me)
                Else
                    Me.left = New SmallFishValue(listener, input)
                End If
                input = left.leftoverInput

                If input.StartsWith(",") Then input = input.Substring(1) 'remove the comma

                If input.StartsWith("[") Then
                    Me.right = New SmallFishPair(listener, input, Me)
                Else
                    Me.right = New SmallFishValue(listener, input)
                End If
                input = right.leftoverInput

                If input.StartsWith("]") Then input = input.Substring(1) ' remove the closing ]

                Me.leftoverInput = input
                listener.Debug("Finished. leftover = '" & Me.leftoverInput & "'")
            End Sub

            Public Sub New(left As SmallFishObject, right As SmallFishObject)
                MyBase.New(left.listener, "")
                Me.left = left
                Me.right = right
            End Sub

            Public Overrides Function ToString() As String
                Return "[{" & level & "}" & left.ToString & "," & right.ToString & "]"
            End Function

            Public Sub Reduce()

                If Me.Level > 4 Then

                End If


            End Sub

        End Class

        Public Shared Function Add(left As SmallFishObject, right As SmallFishObject) As SmallFishPair
            Return New SmallFishPair(left, right)
        End Function

    End Class
End Namespace
