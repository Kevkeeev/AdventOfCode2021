﻿Imports System
Imports System.Collections.Generic
Imports System.Linq

Namespace AdventOfCode2021
    Public Class Day21
        Inherits DayBase

        Public Overrides ReadOnly Property DayNumber As Integer = 21

        Public players As List(Of Player)
        Public die As DeterministicDie
        Public winningThreshold As Integer = 1000

        Public Overrides Function PartOne() As Long
            Prepare()

            'If cDebug Then winningThreshold = 20

            Dim cur As Integer = 0 'start at 0
            Dim round As Integer = 0

            Dim rolls As New List(Of Integer)
            Dim steps As Integer
            Dim newPos As Integer

            While True
                round += 1

                rolls.Clear()
                steps = 0
                For i As Integer = 1 To 3
                    rolls.Add(die.Roll)
                    steps += rolls.Last
                Next

                Debug("Round " & round & ", player " & (cur + 1) & " rolled: " & String.Join(",", rolls) & " = " & steps & "(% 10 = " & steps Mod 10 & ")" & " from " & players(cur).pos & " to " & players(cur).Move(steps) & ", new score: " & players(cur).score)

                If players(cur).score >= winningThreshold Then Exit While

                cur = (cur + 1) Mod players.Count
            End While

            Debug("Victory! Player " & cur + 1 & " won, score = " & players(cur).score)

            cur = (cur + 1) Mod players.Count
            Dim losingScore As Integer = players(cur).score
            Dim dieRolls As Integer = die.rolls


            Debug("Losing score: " & losingScore & " * die rolls: " & dieRolls)
            Return losingScore * dieRolls
        End Function

        Public Overrides Function PartTwo() As Long
            For Each line As String In GetInput()
            Next

            Return -1
        End Function

        Private Sub Prepare()
            Me.die = New DeterministicDie()
            Me.players = New List(Of Player)

            Dim pos As Integer
            For Each line As String In GetInput
                pos = Integer.Parse(line.Substring(line.LastIndexOf(" ") + 1))
                players.Add(New Player(pos))
            Next

            Debug("Players: [" & String.Join(",", players) & "]")
        End Sub

        Public Class Player
            Public pos, score As Integer

            Public Sub New(pos As Integer)
                Me.pos = pos
                Me.score = 0
            End Sub

            Public Function Move(steps As Integer) As Integer
                steps = steps Mod 10 'since 10 steps is a full circle, 10 = basically 0 steps

                Me.pos += steps '
                If Me.pos > 10 Then Me.pos = Me.pos Mod 10 'don't step over position 10, go back to 1.

                Me.score += Me.pos
                Return Me.pos 'new pos
            End Function

            Public Overrides Function ToString() As String
                Return "{p:" & pos & ", s:" & score & "}"
            End Function
        End Class

        Public Class DeterministicDie
            Public rolls, lastValue As Integer

            Public Sub New()
                Me.rolls = 0
                Me.lastValue = 0
            End Sub

            Public Function Roll() As Integer
                Me.rolls += 1

                Me.lastValue += 1
                If Me.lastValue = 101 Then Me.lastValue = 1

                Return Me.lastValue
            End Function
        End Class

    End Class
End Namespace
