﻿Imports System
Imports System.Collections.Generic
Imports System.Linq

Namespace AdventOfCode2021
    Public Class Day24
        Inherits DayBase

        Public Overrides ReadOnly Property DayNumber As Integer = 24

        Public validator As SubmarineModelNumberValidator

        Public Overrides Function PartOne() As Long
            Prepare()

            For modelNumber As Long = 99999976541575 To 11111111111111 Step -1
                If modelNumber.ToString().Contains("0") Then Continue For

                If validator.Validate(modelNumber) Then Return modelNumber
            Next

            Return -1 '
        End Function

        Public Overrides Function PartTwo() As Long
            For Each line As String In GetInput()
            Next

            Return -1
        End Function

        Private Sub Prepare()
            Me.validator = New SubmarineModelNumberValidator(Me)

            For Each line As String In GetInput
                With line.Split(" ")
                    Me.validator.operations.Add(New Operation(.GetValue(0), .GetValue(1), If(.Count > 2, .GetValue(2), "")))
                End With
            Next
        End Sub

        Public Class SubmarineModelNumberValidator
            Public w, x, y, z As Long
            Public operations As List(Of Operation)
            Private listener As Day24

            Public Sub New(listener As Day24)
                Me.operations = New List(Of Operation)
                Me.listener = listener
            End Sub

            Public Function Validate(modelNum As String) As Boolean
                Dim copyNum As String = modelNum 'use this to split the digits off

                Reset()
                For Each op As Operation In operations
                    If op.type = Operation.eOperation.inp Then
                        op.right = copyNum.Substring(0, 1)

                        If copyNum.Length > 1 Then copyNum = copyNum.Substring(1)
                    End If

                    Execute(op)
                Next

                listener.DebugF("Validated " & modelNum & " => z: " & z)
                Return (z = 0)
            End Function

            Private Property Value(var As String) As Long
                Get
                    Select Case var
                        Case "w"
                            Return w
                        Case "x"
                            Return x
                        Case "y"
                            Return y
                        Case "z"
                            Return z
                    End Select
                    Throw New NullReferenceException
                End Get
                Set(value As Long)
                    Select Case var
                        Case "w"
                            w = value
                        Case "x"
                            x = value
                        Case "y"
                            y = value
                        Case "z"
                            z = value
                    End Select
                End Set
            End Property

            Private Sub Reset()
                w = 0
                x = 0
                y = 0
                z = 0
            End Sub

            Private Sub Execute(operation As Operation)
                Dim leftValue As Long = Value(operation.left)

                Dim rightValue As Long
                If Not Long.TryParse(operation.right, rightValue) Then
                    rightValue = Value(operation.right)
                End If

                Dim newValue As Long
                Try
                    Select Case operation.type
                        Case Operation.eOperation.inp
                            newValue = rightValue
                        Case Operation.eOperation.add
                            newValue = leftValue + rightValue
                        Case Operation.eOperation.mul
                            newValue = leftValue * rightValue
                        Case Operation.eOperation.div
                            newValue = leftValue / rightValue
                        Case Operation.eOperation.mod
                            newValue = leftValue Mod rightValue
                        Case Operation.eOperation.eql
                            newValue = If(leftValue = rightValue, 1, 0)
                    End Select
                Catch ex As Exception
                    listener.DebugF("Exception: " & operation.type.ToString & " " & leftValue & " " & rightValue)
                    Throw
                End Try

                listener.Debug("Operation: " & operation.type.ToString & " l: " & operation.left & " = " & leftValue &
                                ", r: " & operation.right & " = " & rightValue & ", new: " & newValue)

                Value(operation.left) = newValue
            End Sub
        End Class

        Public Class Operation
            Public type As eOperation
            Public left As String
            Public right As String

            Public Sub New(type As String, left As String, right As String)
                Select Case type
                    Case "inp"
                        Me.type = eOperation.inp
                    Case "add"
                        Me.type = eOperation.add
                    Case "mul"
                        Me.type = eOperation.mul
                    Case "div"
                        Me.type = eOperation.div
                    Case "mod"
                        Me.type = eOperation.mod
                    Case "eql"
                        Me.type = eOperation.eql
                End Select

                Me.left = left
                Me.right = right
            End Sub

            Enum eOperation
                inp = 1
                add
                mul
                div
                [mod]
                eql
            End Enum
        End Class

    End Class
End Namespace
