﻿Imports System
Imports System.Collections.Generic
Imports System.Linq

Namespace AdventOfCode2021
    Public Class Day4
        Inherits DayBase

        Public Overrides ReadOnly Property DayNumber As Integer = 4

        Dim cards As New List(Of BingoCard)
        Dim balls As New List(Of Integer)

        Public Overrides Function PartOne() As Long
            Me.Prepare()

            Dim bingoCards As New List(Of BingoCard)
            Dim lastBall As Integer = 0

            For Each ball As Integer In balls
                Debug("Ball: " & ball)
                lastBall = ball

                For Each card As BingoCard In cards
                    card.Mark(ball)
                    If card.HasBingo Then bingoCards.Add(card)
                Next

                If bingoCards.Count > 0 Then
                    If bingoCards.Count > 1 Then bingoCards.Sort(Function(y, x) x.GetSumOfUnmarkedNumbers.CompareTo(y.GetSumOfUnmarkedNumbers)) 'We're sorting backwards, largest number at the front.
                    For Each card As BingoCard In bingoCards
                        Debug("BINGO at card " & card.number & "! Score = " & card.GetSumOfUnmarkedNumbers & ", ball = " & lastBall)
                    Next
                    Exit For
                End If
            Next

            Return bingoCards.First.GetSumOfUnmarkedNumbers * lastBall
        End Function

        Public Overrides Function PartTwo() As Long
            Me.Prepare()

            Dim bingoCards As New List(Of BingoCard)
            Dim lastBall As Integer = 0

            For Each ball As Integer In balls
                Debug("Ball: " & ball)
                lastBall = ball

                For Each card As BingoCard In cards
                    card.Mark(ball)
                    If card.HasBingo Then bingoCards.Add(card)
                Next

                If bingoCards.Count > 0 Then
                    If Me.cards.Count > bingoCards.Count Then
                        'there's still some unbingo'ed cards left after this round. Remove al currently bingo'ed cards, since they've won prematurely.
                        For Each bingoCard As BingoCard In bingoCards
                            For i As Integer = 0 To cards.Count - 1 'Loop over the array with removing, without breaking the iterator.
                                If cards(i).number = bingoCard.number Then
                                    Debug("Removing card " & cards(i).number)
                                    Me.cards.RemoveAt(i)
                                    Debug("Now " & Me.cards.Count & " cards left.")
                                    i -= 1 'reset the index
                                    Exit For 'and jump out of this loop
                                End If
                            Next
                        Next
                        bingoCards.Clear()
                    Else
                        'there are no more unbingo'ed cards left. All of the current bingocards have bingo'ed in this round.
                        If bingoCards.Count > 1 Then bingoCards.Sort(Function(y, x) x.GetSumOfUnmarkedNumbers.CompareTo(y.GetSumOfUnmarkedNumbers)) 'We're sorting backwards, largest number at the front.
                        For Each card As BingoCard In bingoCards
                            Debug("BINGO at card " & card.number & "! Score = " & card.GetSumOfUnmarkedNumbers & ", ball = " & lastBall)
                        Next
                        Exit For
                    End If
                End If
            Next

            Return bingoCards.First.GetSumOfUnmarkedNumbers * lastBall
        End Function


        Private Sub Prepare()
            For Each line As String In GetInput()
                If balls.Count = 0 Then
                    For Each number As String In line.Split(",")
                        balls.Add(Integer.Parse(number))
                    Next
                ElseIf String.IsNullOrWhiteSpace(line) Then
                    cards.Add(New BingoCard(Me, cards.Count))
                Else
                    cards.Last.AddLine(line)
                End If
            Next

            If cDebug Then
                Debug("finished reading. Balls = " & String.Join(",", balls))
                For Each card As BingoCard In cards
                    Debug("finished reading. Card = " & card.ToString)
                Next
            End If

        End Sub

        Class BingoCard
            Private card As List(Of List(Of Integer))
            Private marked As List(Of List(Of Boolean))
            Public number As Integer
            Private listener As Day4

            Public Sub New(listener As Day4, i As Integer)
                Me.listener = listener
                number = i
                card = New List(Of List(Of Integer))
                marked = New List(Of List(Of Boolean))
            End Sub

            Public Sub AddLine(line As String)
                card.Add(New List(Of Integer))
                marked.Add(New List(Of Boolean))

                For Each number As String In line.Split(" ")
                    If String.IsNullOrWhiteSpace(number) Then Continue For 'if we have two spaces next to eachother, we get an empty element here.

                    card.Item(card.Count - 1).Add(Integer.Parse(number.Trim))
                    marked.Item(card.Count - 1).Add(False)
                Next
            End Sub

            Public Overrides Function ToString() As String
                Dim str As String = ""
                For Each line As List(Of Integer) In card
                    str &= "{" & String.Join(",", line) & "}"
                Next
                Return str
            End Function

            Public Function GetRow(index As Integer) As List(Of Integer)
                Return card(index)
            End Function

            Public Function GetColumn(index As Integer) As List(Of Integer)
                Dim col As New List(Of Integer)
                For Each row As List(Of Integer) In card
                    col.Add(row(index))
                Next
                Return col
            End Function

            Public Sub Mark(ball As Integer)
                For i As Integer = 0 To card.Count - 1
                    For j As Integer = 0 To card(i).Count - 1
                        If card(i)(j) = ball Then marked(i)(j) = True
                    Next
                Next
            End Sub

            Public Function HasBingo() As Boolean
                Dim bingo As Boolean = True 'unless we dont

                'Check rows
                For i As Integer = 0 To card.Count - 1
                    bingo = True
                    For j As Integer = 0 To card(i).Count - 1
                        'listener.Debug("Checking card " & Me.number & ": num(" & i & ")(" & j & ") = " & card(i)(j) & " > " & If(marked(i)(j), "x", "_"))
                        If Not marked(i)(j) Then
                            bingo = False
                            Continue For
                        End If
                    Next
                    If bingo Then
                        listener.Debug("BINGO on card " & Me.number & ", row " & i & " = " & String.Join(",", GetRow(i)))
                        Return True
                    End If
                Next

                'Check columns
                For j As Integer = 0 To card.First.Count - 1
                    bingo = True
                    For i As Integer = 0 To card.Count - 1
                        If Not marked(i)(j) Then
                            bingo = False
                            Continue For
                        End If
                    Next
                    If bingo Then
                        listener.Debug("BINGO on card " & Me.number & ", column " & j & " = " & String.Join(",", GetColumn(j)))
                        Return True
                    End If
                Next

                Return False
            End Function

            Public Function GetSumOfUnmarkedNumbers() As Integer
                Dim score As Integer
                For i As Integer = 0 To card.Count - 1
                    For j As Integer = 0 To card(i).Count - 1
                        If Not marked(i)(j) Then score += card(i)(j)
                    Next
                Next
                Return score
            End Function

        End Class


    End Class
End Namespace
