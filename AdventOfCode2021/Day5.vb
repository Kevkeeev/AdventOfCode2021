﻿Imports System
Imports System.Collections.Generic

Namespace AdventOfCode2021
    Public Class Day5
        Inherits DayBase

        Public Overrides ReadOnly Property DayNumber As Integer = 5

        Private rooftops As RooftopMap

        Public Overrides Function PartOne() As Long
            Prepare(True)

            Return Me.rooftops.GetNumberOfPointsAboveY(1)
        End Function

        Public Overrides Function PartTwo() As Long
            Prepare(False)

            Return Me.rooftops.GetNumberOfPointsAboveY(1)
        End Function



        Private Sub Prepare(skipDiagonalRoutes As Boolean)
            Me.rooftops = New RooftopMap(Me)

            Dim a, b As Point
            For Each line As String In GetInput()
                With line.Split(" -> ")
                    a = New Point(.GetValue(0).Split(","))
                    b = New Point(.GetValue(1).Split(","))
                End With
                Debug("Found route from " & a.ToString & " to " & b.ToString)

                If skipDiagonalRoutes AndAlso a.X <> b.X AndAlso a.Y <> b.Y Then
                    Debug("Skipping diagonal route " & a.ToString & " -> " & b.ToString)
                    Continue For
                ElseIf a.X > b.X OrElse (a.X = b.X AndAlso a.Y > b.Y) Then
                    Debug("Swapping " & a.ToString & " & " & b.ToString)
                    Swap(a, b) 'we always want to go UP the numbers.
                End If

                Me.rooftops.Mark(New Route(a, b))
            Next

            Debug("Rooftop map created.")
            For Each line As String In Me.rooftops.ToStringMap
                Debug(line)
            Next
        End Sub


        Public Structure Point
            Public X As Integer
            Public Y As Integer

            Public Sub New(x As Integer, y As Integer)
                Me.X = x
                Me.Y = y
            End Sub

            Public Sub New(nums As String())
                Me.X = Integer.Parse(nums(0))
                Me.Y = Integer.Parse(nums(1))
            End Sub

            Public Overrides Function ToString() As String
                Return "(" & Me.X & "," & Me.Y & ")"
            End Function

            Public Sub From(other As Point)
                Me.X = other.X
                Me.Y = other.Y
            End Sub
        End Structure

        Public Structure Route
            Public points As List(Of Point)

            Public Sub New(a As Point, b As Point)
                points = New List(Of Point)

                If a.X = b.X OrElse a.Y = b.Y Then
                    For x As Integer = a.X To b.X
                        For y As Integer = a.Y To b.Y
                            points.Add(New Point(x, y))
                        Next
                    Next
                ElseIf a.y > b.y Then 'Diagonal: topleft to bottomright
                    For x As Integer = a.X To b.X
                        points.Add(New Point(x, a.Y + (a.X - x)))
                    Next
                Else 'Diagonally: the other one.
                    For x As Integer = a.X To b.X
                        points.Add(New Point(x, a.Y - (a.X - x)))
                    Next
                End If
            End Sub

            Public Overrides Function ToString() As String
                Return If(points.Count > 0, String.Join(",", points), "empty")
            End Function
        End Structure

        Private Shared Sub Swap(ByRef a As Point, ByRef b As Point)
            Dim x As New Point()
            x.From(a)
            a.From(b)
            b.From(x)
        End Sub

        Class RooftopMap
            Private map As List(Of List(Of Integer))
            Private listener As Day5

            Public Sub New(listener As Day5)
                Me.listener = listener
                map = New List(Of List(Of Integer))
            End Sub

            Public Sub Mark(r As Route)
                listener.Debug("Marking Route " & r.ToString)

                For Each point As Point In r.points
                    While (map.Count - 1) < point.Y
                        map.Add(New List(Of Integer))
                    End While

                    While (map(point.Y).Count - 1) < point.X
                        map(point.Y).Add(0) 'we start at 0 overlaps at every spot.
                    End While

                    'listener.Debug("Marking " & x & "," & y & " from " & map(y)(x))
                    map(point.Y)(point.X) += 1
                Next
            End Sub

            Public Function ToStringMap() As List(Of String)
                Dim str As New List(Of String)
                Dim ing As String
                For i As Integer = 0 To Me.map.Count - 1
                    ing = i & ": "
                    For Each num As Integer In map(i)
                        ing &= If(num = 0, ".", num) & " "
                    Next
                    str.Add(ing)
                Next
                Return str
            End Function

            Public Function GetNumberOfPointsAboveY(y As Integer)
                Dim result As Integer = 0
                For Each row As List(Of Integer) In map
                    For Each point As Integer In row
                        If point > y Then result += 1
                    Next
                Next
                Return result
            End Function
        End Class

    End Class
End Namespace
