﻿Imports System
Imports System.Collections.Generic

Namespace AdventOfCode2021
    Public Class Day6
        Inherits DayBase

        Public Overrides ReadOnly Property DayNumber As Integer = 6

        Public fishies As List(Of Integer)

        Public Overrides Function PartOne() As Long
            Prepare()

            PerformDayCycles(80)

            Return fishies.Count ' zzzzZZzz
        End Function

        Public Overrides Function PartTwo() As Long
            Prepare()

            PerformDayCycles(256)

            Return fishies.Count ' zzzzZZzz
        End Function

        Private Sub PerformDayCycles(num As Integer)
            Dim time As DateTime = DateTime.Now

            For day As Integer = 1 To num
                For i As Integer = 0 To fishies.Count - 1
                    fishies(i) -= 1
                    If fishies(i) < 0 Then
                        fishies(i) = 6
                        fishies.Add(8)
                    End If
                Next

                If day < 10 Then
                    Debug("Day " & day.ToString("#0") & ": " & String.Join(",", fishies))
                ElseIf day Mod 10 = 0 OrElse day > 200 Then
                    Debug(DateTime.Now().ToString("yyyy-MM-dd HH:mm:ss.fffff") &
                          " - finished day " & day.ToString("##0").PadLeft(3, " ") &
                          " > " & fishies.Count.ToString("# ### ### ##0").PadLeft(14, " ") &
                          " - took: " & (DateTime.Now - time).ToString("mm\:ss\.fffff") & "")
                    time = DateTime.Now 'reset stopwatch.
                End If
            Next
        End Sub

        Private Sub Prepare()
            fishies = New List(Of Integer)

            For Each line As String In GetInput()
                For Each num As String In line.Split(",")
                    fishies.Add(Integer.Parse(num))
                Next
            Next
            Debug("Fishies: " & String.Join(",", fishies))
        End Sub

    End Class
End Namespace
