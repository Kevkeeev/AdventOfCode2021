﻿Imports System
Imports System.Collections.Generic

Namespace AdventOfCode2021
    Public Class Day7
        Inherits DayBase

        Public Overrides ReadOnly Property DayNumber As Integer = 7

        Dim crabs As List(Of Integer)
        Dim minPos, maxPos As Integer

        Public Overrides Function PartOne() As Long
            Prepare()

            Dim moves As New Dictionary(Of Integer, Integer)
            For i As Integer = Me.minPos To Me.maxPos
                moves.Add(i, 0)
                For Each crab As Integer In Me.crabs
                    moves(i) += Math.Abs(crab - i)
                Next
            Next

            Dim lowestMoves As Integer = Integer.MaxValue
            For i As Integer = Me.minPos To Me.maxPos
                If moves(i) < lowestMoves Then lowestMoves = moves(i)
            Next

            Return lowestMoves
        End Function

        Public Overrides Function PartTwo() As Long
            Prepare()

            Dim moves As New Dictionary(Of Integer, Integer)
            For i As Integer = Me.minPos To Me.maxPos
                moves.Add(i, 0)
                For Each crab As Integer In Me.crabs
                    For j As Integer = 1 To Math.Abs(crab - i)
                        moves(i) += j
                    Next
                Next
            Next

            Dim lowestMoves As Integer = Integer.MaxValue
            For i As Integer = Me.minPos To Me.maxPos
                If moves(i) < lowestMoves Then lowestMoves = moves(i)
            Next

            Return lowestMoves
        End Function


        Private Sub Prepare()
            Me.crabs = New List(Of Integer)
            Me.minPos = Integer.MaxValue
            Me.maxPos = Integer.MinValue

            For Each line As String In GetInput()
                For Each num As String In line.Split(",")
                    Dim pos As Integer = Integer.Parse(num)
                    crabs.Add(pos)

                    If minPos > pos Then minPos = pos
                    If maxPos < pos Then maxPos = pos
                Next
            Next
        End Sub

    End Class
End Namespace
