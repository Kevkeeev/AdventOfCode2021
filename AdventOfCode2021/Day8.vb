﻿Imports System
Imports System.Collections.Generic

Namespace AdventOfCode2021
    Public Class Day8
        Inherits DayBase

        Public Overrides ReadOnly Property DayNumber As Integer = 8

        Public Overrides Function PartOne() As Long
            For Each line As String In GetInput()
            Next

            Return -1
        End Function

        Public Overrides Function PartTwo() As Long
            For Each line As String In GetInput()
            Next

            Return -1
        End Function
    End Class
End Namespace
