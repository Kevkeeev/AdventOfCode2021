﻿Imports System.Collections.Generic

Namespace AdventOfCode2021
    Public Class Day9
        Inherits DayBase

        Public Overrides ReadOnly Property DayNumber As Integer = 9

        Public Overrides Function PartOne() As Long
            Dim heightMap As HeightMap = Prepare()
            Dim lowestScore As Integer = 0

            For x As Integer = 0 To heightMap.map.Count - 1
                For y As Integer = 0 To heightMap.map(x).Count - 1
                    If heightMap.IsLowPoint(x, y) Then
                        lowestScore += 1 + heightMap.map(x)(y)
                    End If
                Next
            Next

            Return lowestScore
        End Function

        Public Overrides Function PartTwo() As Long
            For Each line As String In GetInput()
            Next

            Return -1
        End Function

        Private Function Prepare() As HeightMap
            Dim map As New HeightMap(Me)
            For Each line As String In GetInput
                map.AddLine(line)
            Next
            Return map
        End Function

        Public Class HeightMap
            Public map As List(Of List(Of Integer))
            Private listener As Day9

            Public Sub New(listener As Day9)
                Me.listener = listener
                map = New List(Of List(Of Integer))
            End Sub

            Public Sub AddLine(line As String)
                map.Add(New List(Of Integer))
                For i As Integer = 0 To line.Length - 1
                    map(map.Count - 1).Add(Integer.Parse(line.Substring(i, 1)))
                Next
            End Sub

            Public Function IsLowPoint(x As Integer, y As Integer) As Boolean
                listener.Debug("Checking " & x & "," & y & " = " & map(x)(y))

                Dim self As Integer = map(x)(y)
                If self = 9 Then Return False 'Can never be a low point even if surrounded by 9's.

                If y > 0 Then 'check UP
                    If self >= map(x)(y - 1) Then Return False
                End If

                If y < map(x).Count - 1 Then 'check DOWN
                    If self >= map(x)(y + 1) Then Return False
                End If

                If x > 0 Then 'check LEFT
                    If self >= map(x - 1)(y) Then Return False
                End If

                If x < map.Count - 1 Then 'check RIGHT
                    If self >= map(x + 1)(y) Then Return False
                End If

                listener.Debug("Found a low point at: " & x & "," & y & " = " & self)
                Return True
            End Function

        End Class

    End Class
End Namespace
