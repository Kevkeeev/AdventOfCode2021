﻿Imports System
Imports System.Collections.Generic
Imports Microsoft.VisualBasic.Constants

Namespace AdventOfCode2021

    Public MustInherit Class DayBase

        Public ReadOnly cInputFolder As String = "../../../../input/"

        Public ReadOnly Property cInputFile As String
            Get
                Return cInputFolder & "day" & DayNumber & If(cDebug, "-example", "") & ".txt"
            End Get
        End Property

        Private items As List(Of String)
        Public ReadOnly Property GetInput As List(Of String)
            Get
                If items Is Nothing Then
                    items = New List(Of String)
                    For Each item As String In IO.File.ReadAllLines(cInputFile)
                        items.add(item)
                    Next
                End If
                Return items
            End Get
        End Property

        Public Sub Debug(msg As String, Optional newline As String = vbCrLf)
            If cDebug Then DebugF(msg, newline)
        End Sub

        'DebugF = Force.
        Public Sub DebugF(msg As String, Optional newline As String = vbCrLf)
            If msg <> "" AndAlso newline = vbCrLf Then Console.Write(Date.Now.ToString("yyyy-MM-dd HH:mm:ss") & " Day " & DayNumber & ": ")
            Console.Write(msg & newline)
        End Sub

        Public MustOverride ReadOnly Property DayNumber() As Integer

        Public MustOverride Function PartOne() As Long

        Public MustOverride Function PartTwo() As Long

    End Class

End Namespace