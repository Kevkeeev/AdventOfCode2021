Imports System
Imports Microsoft.VisualBasic.Constants

Namespace AdventOfCode2021
    Module Program

        Public cDebug As Boolean = True

        Sub Main(args As String())
            Dim tmp As String
            Dim day, part As Integer
            Do
                Console.Write("Day?       : ")
                tmp = Console.ReadLine
                While Not Integer.TryParse(tmp, day)
                    Console.Write("Day?       : ")
                    tmp = Console.ReadLine
                End While

                Console.Write("Part?      : ")
                tmp = Console.ReadLine
                While Not Integer.TryParse(tmp, part)
                    Console.Write("Part?      : ")
                    tmp = Console.ReadLine
                End While

                Console.Write("Debug? Y/n : ")
                cDebug = UserSaysYes()

                Console.WriteLine("")
                With DayX(day)
                    Select Case part
                        Case 1
                            Console.WriteLine("The answer to Day " & .DayNumber & " - Part One = " & .PartOne())
                        Case 2
                            Console.WriteLine("The answer to Day " & .DayNumber & " - Part Two = " & .PartTWo())
                    End Select
                End With

                Console.Write(vbCrLf & "Try again? Y/n :")
            Loop While Not cDebug AndAlso UserSaysYes() 'If we've debugged the last time, get out anyway.
        End Sub

        Function UserSaysYes() As Boolean
            Dim input As String = Console.ReadLine
            Return (input = "" OrElse input.ToLower.Substring(0, 1).Equals("y"))
        End Function

        Function DayX(number As Integer) As DayBase
            Select Case number
                Case 1
                    Return New Day1
                Case 2
                    Return New Day2
                Case 3
                    Return New Day3
                Case 4
                    Return New Day4
                Case 5
                    Return New Day5
                Case 6
                    Return New Day6
                Case 7
                    Return New Day7
                Case 8
                    Return New Day8
                Case 9
                    Return New Day9
                Case 10
                    Return New Day10
                Case 11
                    Return New Day11
                Case 12
                    Return New Day12
                Case 13
                    Return New Day13
                Case 14
                    Return New Day14
                Case 15
                    Return New Day15
                Case 16
                    Return New Day16
                Case 17
                    Return New Day17
                Case 18
                    Return New Day18
                Case 19
                Case 20
                Case 21
                    Return New Day21
                Case 22
                Case 23
                Case 24
                    Return New Day24
                Case 25
            End Select
            Throw New Exception("Day  " & number & " not recognized!")
        End Function

    End Module

End Namespace