# AdventOfCode2021
Advent of Code 2021 - see www.adventofcode.com

## Progress

| Puzzle | 1..5 | 6   |  7  |  8  |  9  | 10..11 | 12  | 13  | 14  | 15  | 16  | 17  | 18  | 19  | 20  | 21  | 22  | 23  | 24  |  25 |
|---     |:----:|:---:|:---:|:---:|:---:|:------:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|Basics  | :heavy_check_mark:  | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark:           | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    | :heavy_check_mark: |                    |                    |                    |                    |
|Part 1  | :star2:             | :star:             | :star:             |                    | :star:             |:star2:             | :star:                       | :star:             | :star:             | :alarm_clock:      | :star:             |                    |                    |                    |                    | :star:             |                    |                    |                    |                    |
|Part 2  | :star2:             | :alarm_clock:      | :star:             |                    |                    |:star2:             | :chart_with_downwards_trend: | :star:             | :alarm_clock:      |                    | :star:             |                    |                    |                    |                    |                    |                    |                    |                    |                    |

### Legend

| Icon | Meaning |
|---   |---      |
| :alarm_clock: | Not optimized enough to compute. |
| :chart_with_downwards_trend: | Result not entirely correct. |
